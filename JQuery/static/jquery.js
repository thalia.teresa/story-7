
    $(document).ready(function(){
        $(".card-body").hide()
        $(".load").remove()
       
    })

    $(".slider").click(function(){
        if($("body").hasClass("day")){
            $("body").removeClass("day")
            $("body").addClass("night")
            $(".styling").attr("href","/static/style-night.css")
        }
        else{
            $("body").removeClass("night")
            $("body").addClass("day")
            $(".styling").attr("href","/static/style.css")
        }
    })
 
    $(".card-header").click(function(){  
        //self clicking close
        if($(this).next(".card-body").hasClass("active")){
            console.log("a")
            $(this).next(".card-body").removeClass("active").slideUp()
        }
        else{
            $(".card .card-body").removeClass("active").slideUp()
            $(this).next(".card-body").addClass("active").slideDown()
        }
    })
